package com.company.details;

public class Engine {
    private String power; ///"Power"; ///потужність
    private String manufacture; /// = "Manufacture"; ///виробник

    public Engine(String power, String manufacture) {
        this.power = power;
        this.manufacture = manufacture;
    }

    public String getPower() {
        return power;
    }
    public void setPower(String power) {
        this.power = power;
    }

    public String getManufacture() {
        return manufacture;
    }

    public void setManufacture(String manufacture) {
        this.manufacture = manufacture;
    }

    public String toString(){
        return "Power: " + getPower() + ", Manufacture: " + getManufacture() + ".";
    }

}
