package com.company.professions;

import com.company.entities.Person;

public class Driver extends Person {
    private int experience; /// стаж_водіння

    public Driver(String name, String surname, String patronymic, int age, String sex, String telephone, int experience) {
        super(name, surname, patronymic, age, sex, telephone);
        this.experience = experience;
    }
    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public String toString() {
        return "Experience: " + getExperience() + ". " + getPerson() + ". ";
    }
}
