package com.company;

import com.company.details.Engine;
import com.company.professions.Driver;
import com.company.vehicles.Car;
import com.company.vehicles.Lorry;
import com.company.vehicles.SportCar;

public class InfoCar {

    public static void main(String[] args) {
        Driver mercDriver = new Driver("Ivanov", "Anatoliy", "Petrovich", 35,
                "Man", "+380676749032", 10);
        Engine mercEngine = new Engine("330", "Mercedes");
        Car car = new Car("Mercedes", "GLS", 2500, mercDriver, mercEngine);

        Driver lorryDriver = new Driver( "Vasechkin", "Vasiliy", "Nikolaevich", 27,
                "Man", "+380505399999", 6);
        Engine lorryEngine = new Engine("190", "Iveco");
        Lorry lorry = new Lorry("Iveco", "E", 8000, lorryDriver, lorryEngine, 20000);

        Driver sportDriver = new Driver("Kira", "Petrova", "Anatolievna", 32,
                "Woman", "+380507897777",7);
        Engine sportEngine = new Engine("550", "Ferrary");
        SportCar sportCar = new SportCar("Ferrary", "S", 1700, sportDriver, sportEngine, 387);

        System.out.println(car);
        System.out.println(lorry);
        System.out.println(sportCar);

        car.start();
        car.turnLeft();
        car.turnRight();
        car.stop();
    }
}