package com.company.entities;

import com.company.professions.Driver;
import com.company.entities.Person;

public class Person {

    private String name;
    private String surname;
    private String patronymic;
    private int age;
    private String sex;
    private String telephone;

    public Person(String name, String surname, String patronymic, int age, String sex, String telephone) {
        this.name = name; // Прізвище
        this.surname = surname; // ім'я
        this.patronymic = patronymic; // по-батькові;
        this.age = age; // в майбутньому буде int
        this.sex = sex; // стать
        this.telephone = telephone; //номер телефону
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    public String getPerson() {
        return "Person, FullName: " + getName() + ", " + getSurname() + ", " + getPatronymic() +
                ", Age: " + getAge() + ", Sex: " + getSex() + ", Telephone: " + getTelephone();
    }
    public String toString() {
        return getPerson();
    }
}
