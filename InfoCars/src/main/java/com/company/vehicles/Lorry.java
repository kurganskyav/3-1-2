package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class Lorry extends Car {
    private int cargo;
    public Lorry(String mark, String clCar, int weight, Driver driver, Engine engine, int cargo) {
        super(mark, clCar, weight, driver, engine);
        this.cargo = cargo;
    }
    public int getCargo() {
        return cargo;
    }
    public void setCargo(int cargo) {
        this.cargo = cargo;
    }
    public String toString() {
        return "Lorry, Cargo: " + getCargo() + ". " + super.toString();
    }
}
