package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class Car {
    private String mark;
    private String clCar;
    private int weight;
    private Driver driver;
    private Engine engine;

    public Car(String mark, String clCar, int weight, Driver driver, Engine engine) {
        this.mark = mark;
        this.clCar = clCar;
        this.weight = weight;
        this.driver = driver;
        this.engine = engine;
    }

    public String getMark() {
        return mark;
    }
    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getClCar() {
        return clCar;
    }
    public void setClCar(String clCar) {
        this.clCar = clCar;
    }

    public int getWeight() {
        return weight;
    }
    public void setWeight(int weight) {
        this.weight = weight;
    }

    public Driver getDriver() {
        return driver;
    }
    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Engine getEngine() {
        return engine;
    }
    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public void start() {
        System.out.println("Поїхали!");
    }
    public void stop() {
        System.out.println("Зупиняємось!");
    }
    public void turnRight() {
        System.out.println("Поворот направо!");
    }
    public void turnLeft() {
        System.out.println("Поворот наліво!");
    }

    public String toString() {
        return "Car, Mark: " + getMark() + ", Class: " + getClCar() + ", Weight: " + getWeight() +
                ", Driver: " + getDriver() + "Engine: " + getEngine();
    }
}