package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class SportCar extends Car {
    private int speed;
    public SportCar(String mark, String clCar, int weight, Driver driver, Engine engine, int speed) {
        super(mark,clCar,weight,driver,engine);
        this.speed = speed;
    }
    public int getSpeed() {
        return speed;
    }
    public void setSpeed(int speed) {
        this.speed = speed;
    }
    public String toString() {
        return "SportCar, Speed: " + getSpeed() + ". " + super.toString();
    }
}
